# noTCPShell

**noTCPShell** se trata de un programa construido en el lenguaje de programación _Bash_, ideal para ser usado sobre casos en los que no logramos entablar sesiones remotas TCP sobre nuestro equipo desde un sistema vulnerado.

Esto en ocasiones suele deberse a las reglas de firewall implementadas:

![noTCPShell](Imagenes/ReverseShell.png)

Donde para el esquema definido, logramos ejecutar comandos sobre el sistema afectado aprovechando las vulnerabilidades presentes pero no conseguimos el acceso al mismo vía netcat. 

Con la funcionalidad del script, <b>evitamos tener que hacer uso de este tipo de conexiones</b>, pudiendo directamente desde la herramienta gestionar la intrusión siendo capaces incluso de manejar el input de contraseñas así como las sesiones interactivas de programas como python,mysql, etc.

Demo de la herramienta:

[Demo Herramienta - Youtube](https://www.youtube.com/watch?v=DfYiMSx8Wjg)


